// eslint-disable-next-line import/prefer-default-export
export const newsData = [
  {
    img: './static/images/content/newscontent/content1.png',
    title: '10 Ekstensi terbaik untuk Visual Studio Code',
    category: 'Enginering',
    date: '18 Mei 2020',
  },
  {
    img: './static/images/content/newscontent/content3.png',
    title: 'Text Editor Terbaik Untuk Ngoding di Tahun 2020',
    category: 'Enginering',
    date: '18 Mei 2020',
  },
  {
    img: './static/images/content/newscontent/content2.png',
    title: 'Mengelola Project Dengan Lebih Mudah Menggunakan GIT',
    category: 'Enginering',
    date: '18 Mei 2020',
  },
]
