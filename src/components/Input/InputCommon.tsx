import React from 'react'
import { Form, Input } from 'antd'

interface InputCommonProps {
  name: string
  className?: string
  label: string
  placeholder: string
  messages: string
  required: boolean
  type: string
  value?: any
  onChange?: any
}

function InputCommon(props: InputCommonProps) {
  const {
    name,
    className,
    placeholder,
    label,
    messages,
    required,
    type,
    value,
    onChange,
  } = props
  return (
    <div>
      <span className="flex">
        <p className="text-[#404258] text-[14px] font-semibold">{label}</p>
        {required && (
          <p className="text-red-500 ml-1 text-[12px] md:text-[12px]">*</p>
        )}
      </span>
      <Form.Item
        className="mb-7"
        name={name}
        rules={[{ required, message: messages }]}
      >
        <Input
          type={type}
          placeholder={placeholder}
          className={className}
          value={value}
          onChange={onChange}
        />
      </Form.Item>
    </div>
  )
}

export default InputCommon
